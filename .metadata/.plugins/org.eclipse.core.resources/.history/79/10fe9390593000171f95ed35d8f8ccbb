package serverCode;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JLabel;

import com.sun.jna.Native;
import com.sun.jna.platform.unix.X11.Window;
import com.sun.jna.platform.win32.WinDef.HWND;
import com.sun.jna.win32.StdCallLibrary;
import com.sun.jna.win32.W32APIOptions;

import shared.AppData;

/**
 * @author Darren White
 * @version 1.0
 */
public class Overlay
{

	private String windowName;
	private static JFrame frame;
	private static int[] rectangle;

	/**
	 * Constructor for semi transparent overlay component
	 * 
	 * @param windowName
	 */
	public Overlay(String windowName)
	{
		this.windowName = windowName;
		// Create a new frame (the overlay)
		this.frame = new JFrame(this.windowName + " Overlay");
		int[] rect;
		try
		{
			rect = Overlay.getRect(this.windowName);
			this.rectangle = rect;
			AppData.WINDOW_NAME = this.windowName;
			System.out.printf("The corner locations for the window \"%s\" are %s", windowName, Arrays.toString(rect));
			displayOverlay(rect);
		} catch (WindowNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (GetWindowRectException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @source https://github.com/Toms42/sweep-ai/blob/master/src/main/java/
	 *         minesweeper/GetWindowRect.java
	 * @date 2nd Feb 2017
	 */
	public interface User32 extends StdCallLibrary
	{
		User32 INSTANCE = (User32) Native.loadLibrary("user32", User32.class, W32APIOptions.DEFAULT_OPTIONS);

		HWND FindWindow(String lpClassName, String lpWindowName);

		int GetWindowRect(HWND handle, int[] rect);
	}

	/**
	 * 
	 * @source https://github.com/Toms42/sweep-ai/blob/master/src/main/java/
	 *         minesweeper/GetWindowRect.java
	 * @date 2nd Feb 2017
	 * 
	 *       Gets the location and dimensions of an active window on screen and
	 *       adds them to an array.
	 * 
	 * @param windowName
	 * @return
	 * @throws WindowNotFoundException
	 * @throws GetWindowRectException
	 */
	public static int[] getRect(String windowName) throws WindowNotFoundException, GetWindowRectException
	{
		// Gets a handle to a window
		HWND hwnd = User32.INSTANCE.FindWindow(null, windowName);

		// Check if window is open
		if (hwnd == null)
		{
			throw new WindowNotFoundException("", windowName);
		}

		int[] rect =
		{ 0, 0, 0, 0 };

		int result = User32.INSTANCE.GetWindowRect(hwnd, rect);
		if (result == 0)
		{
			throw new GetWindowRectException(windowName);
		}

		return rect;
	}

	/**
	 * @source https://github.com/Toms42/sweep-ai/blob/master/src/main/java/
	 *         minesweeper/GetWindowRect.java
	 * @date 2nd Feb 2017
	 */
	@SuppressWarnings("serial")
	public static class WindowNotFoundException extends Exception
	{
		public WindowNotFoundException(String className, String windowName)
		{
			super(String.format("Window null for className: %s; windowName: %s", className, windowName));
		}
	}

	/**
	 * @source https://github.com/Toms42/sweep-ai/blob/master/src/main/java/
	 *         minesweeper/GetWindowRect.java
	 * @date 2nd Feb 2017
	 */
	@SuppressWarnings("serial")
	public static class GetWindowRectException extends Exception
	{
		public GetWindowRectException(String windowName)
		{
			super("Window Rect not found for " + windowName);
		}
	}

	/**
	 * Draws semi transparent overlay on to chosen window
	 * 
	 * @param rect
	 */
	private void displayOverlay(int[] rect)
	{
		// Gets the dimension of the users display
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

		// From the dimension obtained above - get width and height
		double width = screenSize.getWidth();
		double height = screenSize.getHeight();

		// Set's the window to be "always on top"
		frame.setAlwaysOnTop(true);

		frame.setLocationByPlatform(true);

		// Removes title bar & users ability to move + resize
		frame.setUndecorated(true);
		frame.pack();

		frame.setVisible(true);
		frame.setBounds(rect[0] + 10, rect[1] + 5, rect[2] - rect[0] - 20, rect[3] - rect[1] - 20);
		frame.setOpacity(0.1f);

		frame.setAlwaysOnTop(true);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Best Method as of yet for getting screen capture excluding semi
		// transparent overlay
		ScreengrabThread R1 = new ScreengrabThread("Thread-1");
		R1.start();
		System.out.println("Done Thread --->");
	}

	public static void takeScreenGrab() throws GetWindowRectException, WindowNotFoundException
	{
		// Gets a handle to a window
		HWND hwnd = User32.INSTANCE.FindWindow(null, AppData.WINDOW_NAME);

		// Check if window is open
		if (hwnd == null)
		{
			throw new WindowNotFoundException("", AppData.WINDOW_NAME);
		}

		int[] rect =
		{ 0, 0, 0, 0 };

		int result = User32.INSTANCE.GetWindowRect(hwnd, rect);
		if (result == 0)
		{
			throw new GetWindowRectException(AppData.WINDOW_NAME);
		}

		getPartialScreenCapture(AppData.WINDOW_NAME, rect[0], rect[1], rect[2], rect[3]);
	}

	/**
	 * Gets a screen capture of a window using native calls to find its location
	 * and dimensions. Saves capture to disk
	 * 
	 * @param appName
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 */
	private static void getPartialScreenCapture(String appName, int x, int y, int width, int height)
	{
		// Turn overlay off
		frame.setVisible(false);
		// Source:
		// http://www.codejava.net/java-se/graphics/how-to-capture-screenshot-programmatically-in-java
		try
		{
			Robot robot = new Robot();
			String format = "jpg";
			String fileName = "C:\\xampp\\htdocs\\ScreenCapDisp3\\1" + "." + format;

			/*
			 * @source http://www.codejava.net/java-se/graphics/how-to-capture-
			 * screenshot-programmatically-in-java
			 * 
			 * @date 2nd Feb 2017
			 */
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			Rectangle captureRect = new Rectangle(x, y, width, height);
			BufferedImage screenFullImage = robot.createScreenCapture(captureRect);
			ImageIO.write(screenFullImage, format, new File(fileName));

			System.out.println("A partial screenshot saved!");
		} catch (AWTException | IOException ex)
		{
			System.err.println(ex);
		}
		// End
		// Turn overlay on
		frame.setVisible(true);

	}

}
