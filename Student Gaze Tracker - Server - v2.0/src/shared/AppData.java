package shared;

public class AppData
{
	public static String WINDOW_NAME = "";
	
	public static int SCREENGRAB_CAPTURE_RATE_IN_MILLIS = 2000;
	
	public static final int PORT = 5632;
	
	public static final String EMAIL_REGEX_PATTERN = "[A-Za-z]+\\@dkit\\.ie";
}
