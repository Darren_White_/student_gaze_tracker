package serverCode;

/**
 * @author Darren White
 * @version 1.0
 */
public class ProgramWindow
{
	private String name;
	short xCoordinate, yCoordinate, width, height;

	/**
	 * @param name
	 * @param xCoordinate
	 * @param yCoordinate
	 * @param width
	 * @param height
	 */
	public ProgramWindow(String name, short xCoordinate, short yCoordinate, short width, short height)
	{
		this.name = name;
		this.xCoordinate = xCoordinate;
		this.yCoordinate = yCoordinate;
		this.width = width;
		this.height = height;
	}

	/**
	 * @return
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @return
	 */
	public short getxCoordinate()
	{
		return xCoordinate;
	}

	/**
	 * @param xCoordinate
	 */
	public void setxCoordinate(short xCoordinate)
	{
		this.xCoordinate = xCoordinate;
	}

	/**
	 * @return
	 */
	public short getyCoordinate()
	{
		return yCoordinate;
	}

	/**
	 * @param yCoordinate
	 */
	public void setyCoordinate(short yCoordinate)
	{
		this.yCoordinate = yCoordinate;
	}

	/**
	 * @return
	 */
	public short getWidth()
	{
		return width;
	}

	/**
	 * @param width
	 */
	public void setWidth(short width)
	{
		this.width = width;
	}

	/**
	 * @return
	 */
	public short getHeight()
	{
		return height;
	}

	/**
	 * @param height
	 */
	public void setHeight(short height)
	{
		this.height = height;
	}

	@Override
	public String toString()
	{
		return "ProgramWindow [name=" + name + ", xCoordinate=" + xCoordinate + ", yCoordinate=" + yCoordinate
				+ ", width=" + width + ", height=" + height + "]";
	}

}
