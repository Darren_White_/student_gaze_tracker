package serverCode;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;

import shared.AppData;
import shared.Gaze;
import shared.SerializationUtility;
import view.LecturerToolbar;

/**
 * @author Darren White
 * @version 1.0
 */
public class Server
{
	public void start()
	{
		startToolbar();
		startOverlay();
		startServer();
	}

	private void startToolbar()
	{
		new LecturerToolbar().setVisible(true);;
	}

	/**
	 * Loads the overlay component
	 */
	private void startOverlay()
	{
		ArrayList<String> windowList = (ArrayList<String>) WindowNameFinder.getAllWindowNames();

		ArrayList<String> newList = new ProgramFilter(windowList).filter();

		System.out.println("\n\n\n" + newList + "\n\n\n");

		Overlay o = new Overlay(newList.get(0));
		System.out.println("Overlay Running!");
	}

	/**
	 * Starts the server listening for packets from clients
	 */
	private void startServer()
	{

		DatagramSocket serverSocket;
		// Create DatagramSocket object (used for UDP protocol) and pass in
		// agreed PORT
		try
		{
			serverSocket = new DatagramSocket(AppData.PORT);
			System.out.println("Server Now Running!");
		} catch (SocketException e1)
		{
			System.out.println("Failed to create DatagramSocket");
			return;
		}

		// Create two byte arrays to store incoming and outgoing data
		byte[] incomingData = new byte[1024];
		byte[] outgoingData = new byte[1024];

		while (true)
		{
			DatagramPacket incomingPacket = new DatagramPacket(incomingData, incomingData.length);

			// Receive incoming data from client and print it to the screen
			try
			{
				serverSocket.receive(incomingPacket);

				Gaze g = (Gaze) SerializationUtility.unserialize(incomingPacket.getData());
				System.out.println("RECEIVED:> " + g.getStudentId() + " with (" + g.getxCoordinate() + ", "
						+ g.getyCoordinate() + ")");

				// Get the IP address of the client
				InetAddress IPAddress = incomingPacket.getAddress();
				int port = incomingPacket.getPort();

				// Do something to the clients message then convert it into
				// bytes
				g.setxCoordinate((short) (g.getxCoordinate() + 10));

				outgoingData = SerializationUtility.serialize(g);

				// Bundle data into a packet and send it to the client
				DatagramPacket sendPacket = new DatagramPacket(outgoingData, outgoingData.length, IPAddress, port);
				serverSocket.send(sendPacket);
			} catch (IOException e)
			{
				closeSocket(serverSocket);
			}
		}
	}

	/**
	 * Closes the servers datagramsocket
	 * 
	 * @param serverSocket
	 */
	private void closeSocket(DatagramSocket serverSocket)
	{
		if (serverSocket != null && !serverSocket.isClosed())
		{
			serverSocket.close();
		}
	}

}
