package serverCode;

import serverCode.Overlay.GetWindowRectException;
import serverCode.Overlay.WindowNotFoundException;
import shared.AppData;

public class ScreengrabThread implements Runnable
{
	private Thread t;
	private String threadName;

	ScreengrabThread(String name)
	{
		threadName = name;
	}

	public void run()
	{
		try
		{
			while (true)
			{
				Overlay.takeScreenGrab();
				// Let the thread sleep for a while.
				Thread.sleep(AppData.SCREENGRAB_CAPTURE_RATE_IN_MILLIS);
			}
		} catch (InterruptedException e)
		{
			System.out.println("Thread " + threadName + " interrupted.");
		} catch (GetWindowRectException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WindowNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void start()
	{
		if (t == null)
		{
			t = new Thread(this, threadName);
			t.start();
		}
	}
}
