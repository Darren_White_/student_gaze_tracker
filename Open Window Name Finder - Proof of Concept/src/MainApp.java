import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.win32.StdCallLibrary;

/**
 * @author Darren White
 * @version 1.0
 */
public class MainApp
{
	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		MainApp theApp = new MainApp();
		theApp.start();
	}

	/**
	 * 
	 */
	private void start()
	{
		getAllProcesses();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		getAllWindows();
	}

	/**
	 * 
	 */
	private void getAllProcesses()
	{
		try
		{
			String line;
			Process p = Runtime.getRuntime().exec(System.getenv("windir") + "\\system32\\" + "tasklist.exe");
			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = input.readLine()) != null)
			{
				System.out.println(line); // <-- Parse data here.
			}
			input.close();
		} catch (Exception err)
		{
			err.printStackTrace();
		}
	}

	/**
	 * 
	 */
	private void getAllWindows()
	{
		List<String> winNameList = getAllWindowNames();
		for (String winName : winNameList)
		{
			System.out.println(winName);
		}
	}

	/**
	 * @author Darren White
	 *
	 */
	static interface User32 extends StdCallLibrary
	{
		User32 INSTANCE = (User32) Native.loadLibrary("user32", User32.class);

		interface WNDENUMPROC extends StdCallCallback
		{
			boolean callback(Pointer hWnd, Pointer arg);
		}

		boolean EnumWindows(WNDENUMPROC lpEnumFunc, Pointer userData);

		int GetWindowTextA(Pointer hWnd, byte[] lpString, int nMaxCount);

		Pointer GetWindow(Pointer hWnd, int uCmd);
	}

	/**
	 * @return windowNames
	 */
	public static List<String> getAllWindowNames()
	{
		final List<String> windowNames = new ArrayList<String>();
		final User32 user32 = User32.INSTANCE;
		user32.EnumWindows(new User32.WNDENUMPROC()
		{

			@Override
			public boolean callback(Pointer hWnd, Pointer arg)
			{
				byte[] windowText = new byte[512];
				user32.GetWindowTextA(hWnd, windowText, 512);
				String wText = Native.toString(windowText).trim();
				if (!wText.isEmpty())
				{
					windowNames.add(wText);
				}
				return true;
			}
		}, null);

		return windowNames;
	}

}
