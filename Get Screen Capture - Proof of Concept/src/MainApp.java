import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * @author Darren White
 * @version 1.0
 */
public class MainApp
{

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		MainApp theApp = new MainApp();
		theApp.start();
	}

	/**
	 * 
	 */
	private void start()
	{
		getFullScreenCapture();
		getPartialScreenCapture();
	}

	/**
	 * 
	 */
	private void getFullScreenCapture()
	{
		// Source:
		// http://www.codejava.net/java-se/graphics/how-to-capture-screenshot-programmatically-in-java
		try
		{
			Robot robot = new Robot();
			String format = "jpg";
			String fileName = "FullScreenshot." + format;

			Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
			BufferedImage screenFullImage = robot.createScreenCapture(screenRect);
			ImageIO.write(screenFullImage, format, new File(fileName));

			System.out.println("A full screenshot saved!");
		} catch (AWTException | IOException ex)
		{
			System.err.println(ex);
		}
		// End
	}

	/**
	 * 
	 */
	private void getPartialScreenCapture()
	{
		// Source:
		// http://www.codejava.net/java-se/graphics/how-to-capture-screenshot-programmatically-in-java
		try
		{
			Robot robot = new Robot();
			String format = "jpg";
			String fileName = "PartialScreenshot." + format;

			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			Rectangle captureRect = new Rectangle(0, 0, screenSize.width / 2, screenSize.height / 2);
			BufferedImage screenFullImage = robot.createScreenCapture(captureRect);
			ImageIO.write(screenFullImage, format, new File(fileName));

			System.out.println("A partial screenshot saved!");
		} catch (AWTException | IOException ex)
		{
			System.err.println(ex);
		}
		// End

	}

}
