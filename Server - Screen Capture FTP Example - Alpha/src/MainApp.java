import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.net.ftp.FTPClient;

public class MainApp
{
	public static void main(String[] args)
	{
		MainApp theApp = new MainApp();
		try
		{
			theApp.start();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void start() throws MalformedURLException, IOException
	{
		InputStream input = new URL("http://i.stack.imgur.com/IGq0o.png").openStream();
		FTPClient ftp = new FTPClient();
		ftp.connect("localhost", 80);
		ftp.login("root", "");
		ftp.storeFile("Images/image.png", input); // <-- Here, it's just InputStream.
		ftp.logout();
	}
}
