import java.awt.Color;
import java.io.File;
import java.io.IOException;

import org.tc33.jheatchart.HeatChart;

//source("http://www.javaheatmap.com/examples")
public class MainApp
{
	public static void main(String[] args)
	{
		// Create some dummy data.
		double[][] data = new double[][]
		{
				{ 3, 2, 3, 4, 5, 6 },
				{ 2, 3, 4, 5, 6, 7 },
				{ 3, 4, 5, 6, 7, 6 },
				{ 4, 5, 6, 7, 6, 5 } };

		// Step 1: Create our heat map chart using our data.
		HeatChart map = new HeatChart(data);

		// Step 2: Customise the chart.
		map.setHighValueColour(Color.RED);
		map.setLowValueColour(Color.GREEN);
		map.setChartMargin(0);

		// Step 3: Output the chart to a file.
		try
		{
			map.saveToFile(new File("java-heat-chart.png"));
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
