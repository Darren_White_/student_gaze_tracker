package serverCode;

import java.util.ArrayList;

import shared.Gaze;

/**
 * @author Darren White
 * @version 1.0
 */
public class GazeStore
{
	private String studentId;
	private ArrayList<Gaze> gazeList;

	/**
	 * @param studentId
	 */
	public GazeStore(String studentId)
	{
		this.studentId = studentId;
		this.gazeList = new ArrayList<Gaze>();
	}

	/**
	 * Adds a gaze record to the store
	 * 
	 * @param g
	 * @return
	 */
	public boolean addGaze(Gaze g)
	{
		if (!gazeList.contains(g))
		{
			this.gazeList.add(g);
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @return
	 */
	public String getStudentId()
	{
		return studentId;
	}

	/**
	 * @param studentId
	 */
	public void setStudentId(String studentId)
	{
		this.studentId = studentId;
	}

	/**
	 * @return
	 */
	public ArrayList<Gaze> getGazeList()
	{
		return gazeList;
	}

	/**
	 * @param gazeList
	 */
	public void setGazeList(ArrayList<Gaze> gazeList)
	{
		this.gazeList = gazeList;
	}

	@Override
	public String toString()
	{
		return "GazeStore [studentId=" + studentId + ", gazeList=" + gazeList + "]";
	}

}
