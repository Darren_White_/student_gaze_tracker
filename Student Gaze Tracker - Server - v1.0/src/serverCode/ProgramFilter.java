package serverCode;

import java.util.ArrayList;

/**
 * @author Darren White
 * @version 1.0
 */
public class ProgramFilter
{
	private ArrayList<String> programList;

	/**
	 * Constructor for filter component
	 * 
	 * @param programList
	 */
	public ProgramFilter(ArrayList<String> programList)
	{
		this.programList = programList;
	}

	/**
	 * Filters out unwanted window names, such as; services, background
	 * processes etc
	 * 
	 * @return newList
	 */
	public ArrayList<String> filter()
	{
		// Create a new list to hold desirable values
		ArrayList<String> newList = new ArrayList<String>();

		for (int i = 0; i < this.programList.size(); i++)
		{
			// Condition below to be improved in alpha
			if (this.programList.get(i).contains("Eclipse") && this.programList.get(i).contains("Java"))
			{
				System.out.println("Added:> '" + this.programList.get(i) + "' to Program List");
				newList.add(this.programList.get(i));
			}
		}
		return newList;
	}
}