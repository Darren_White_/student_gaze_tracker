package serverCode;

import java.util.ArrayList;
import java.util.List;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.win32.StdCallLibrary;

public class WindowNameFinder
{

	/**
	 * Gets a list of all active windows open, including background processes
	 * and services
	 */
	private List<String> getAllWindows()
	{
		return getAllWindowNames();
	}

	/**
	 * @source https://groups.google.com/forum/#!msg/jna-users/LEWqabitMUM/oUWU-
	 *         OwClmwJ
	 * @date 02 Feb 2017
	 * @version 1.0
	 */
	static interface User32 extends StdCallLibrary
	{
		User32 INSTANCE = (User32) Native.loadLibrary("user32", User32.class);

		interface WNDENUMPROC extends StdCallCallback
		{
			boolean callback(Pointer hWnd, Pointer arg);
		}

		boolean EnumWindows(WNDENUMPROC lpEnumFunc, Pointer userData);

		int GetWindowTextA(Pointer hWnd, byte[] lpString, int nMaxCount);

		Pointer GetWindow(Pointer hWnd, int uCmd);
	}

	/**
	 * @source https://github.com/mirraj2/UI-Scraper/blob/master/src/main/java/
	 *         scraper/WindowsAPI.java
	 * @date 02 Feb 2017 Uses native calls to get all open window names
	 * 
	 * @return windowNames
	 */
	public static List<String> getAllWindowNames()
	{
		final List<String> windowNames = new ArrayList<String>();
		final User32 user32 = User32.INSTANCE;
		user32.EnumWindows(new User32.WNDENUMPROC()
		{

			@Override
			public boolean callback(Pointer hWnd, Pointer arg)
			{
				byte[] windowText = new byte[512];
				user32.GetWindowTextA(hWnd, windowText, 512);
				String wText = Native.toString(windowText).trim();
				if (!wText.isEmpty())
				{
					windowNames.add(wText);
				}
				return true;
			}
		}, null);

		return windowNames;
	}

}
