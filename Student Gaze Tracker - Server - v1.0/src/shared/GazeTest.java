package shared;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

public class GazeTest
{
	private Gaze gaze;

	@Test
	public void testGetXCoordinate()
	{
		Gaze gaze = new Gaze("D00155619", (short) 10, (short) 20, new Date());
		assertEquals("Result", 10, gaze.getxCoordinate());
		gaze = null;
	}

	@Test
	public void testGetYCoordinate()
	{
		Gaze gaze = new Gaze("D00155619", (short) 10, (short) 20, new Date());
		assertEquals("Result", 20, gaze.getyCoordinate());
		gaze = null;
	}

	@Test
	public void testGetStudentID()
	{
		Gaze gaze = new Gaze("D00155619", (short) 10, (short) 20, new Date());
		assertEquals("Result", "D00155619", gaze.getStudentId());
		gaze = null;
	}

	@Test
	public void testGetDate()
	{
		SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
		String dateInString = "02-03-2017 10:20:56";
		Date date = null;
		try
		{
			date = sdf.parse(dateInString);
		} catch (ParseException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		Gaze gaze = new Gaze("D00155619", (short) 10, (short) 20, date);
		assertEquals("Result", date, gaze.getDate());
	}

}
