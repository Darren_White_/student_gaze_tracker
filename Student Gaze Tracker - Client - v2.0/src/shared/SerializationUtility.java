// Modified SerializationUtility file from second year Advanced Object Oriented programming (Years 2014-2015)
// Original Author: Niall McGuinness
package shared;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class SerializationUtility
{
	public static byte[] serialize(Object obj)
	{
		try
		{
			// handle to a Byte Array output stream
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			// handle to an object output stream
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			// write the object to the stream
			oos.writeObject(obj);
			baos.close();
			oos.close();
			byte[] serializedObj = baos.toByteArray();
			return serializedObj;
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public static Object unserialize(byte[] serializedObj)
	{
		Object obj = null;

		try
		{
			ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(serializedObj));
			obj = (Object) ois.readObject();
			ois.close();

		} catch (Exception e)
		{
			e.printStackTrace();
		}

		return obj;
	}

}
