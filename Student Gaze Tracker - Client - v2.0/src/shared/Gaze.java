package shared;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Darren White
 * @version 1.1
 */
public class Gaze implements Serializable
{
	private static final long serialVersionUID = 168875583652344056L;
	private String studentId;
	private short xCoordinate, yCoordinate;
	private Date date;

	/**
	 * @param studentId
	 * @param xCoordinate
	 * @param yCoordinate
	 * @param date
	 */
	public Gaze(String studentId, short xCoordinate, short yCoordinate, Date date)
	{
		this.studentId = studentId;
		this.xCoordinate = xCoordinate;
		this.yCoordinate = yCoordinate;
		this.date = date;
	}

	/**
	 * 
	 */
	public Gaze()
	{
		this.studentId = "";
		this.xCoordinate = 0;
		this.yCoordinate = 0;
		this.date = null;
	}

	/**
	 * @return
	 */
	public String getStudentId()
	{
		return studentId;
	}

	/**
	 * @param studentId
	 */
	public void setStudentId(String studentId)
	{
		this.studentId = studentId;
	}

	/**
	 * @return
	 */
	public short getxCoordinate()
	{
		return xCoordinate;
	}

	/**
	 * @param xCoordinate
	 */
	public void setxCoordinate(short xCoordinate)
	{
		this.xCoordinate = xCoordinate;
	}

	/**
	 * @return
	 */
	public short getyCoordinate()
	{
		return yCoordinate;
	}

	/**
	 * @param yCoordinate
	 */
	public void setyCoordinate(short yCoordinate)
	{
		this.yCoordinate = yCoordinate;
	}

	/**
	 * @return
	 */
	public Date getDate()
	{
		return date;
	}

	/**
	 * @param date
	 */
	public void setDate(Date date)
	{
		this.date = date;
	}

	@Override
	public String toString()
	{
		return "Gaze [studentId=" + studentId + ", xCoordinate=" + xCoordinate + ", yCoordinate=" + yCoordinate
				+ ", date=" + date + "]";
	}

}
