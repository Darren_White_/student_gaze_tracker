package shared;

public class AppData
{
	public static final String LECTURER_IP = "localhost";
	public static final int PORT = 5632;

	public static final String EMAIL_REGEX_PATTERN = "[D]{1}[0-9]{8}\\@student\\.dkit\\.ie";
	public static final String IP_ADDRESS_REGEX_PATTERN = "([0-9]{1,3}\\.){3}[0-9]{1,3}";
}
