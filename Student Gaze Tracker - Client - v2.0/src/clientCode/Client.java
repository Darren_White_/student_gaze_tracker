package clientCode;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

import shared.AppData;
import shared.Gaze;
import shared.SerializationUtility;

/**
 * @author Darren White
 * @version 1.0
 */
public class Client
{

	private Scanner kb = new Scanner(System.in);

	/**
	 * 
	 */
	public void start()
	{
		getGazeData();
	}

	/**
	 * 
	 */
	private void getGazeData()
	{
		// Print out the app header and user menu
		printHeader();
		printMenu();

		// Initialize variables outside of while loop
		String input = "";
		String studentId = "";
		short x, y;
		Date date;
		Gaze g = new Gaze();

		// Loop until user wishes to stop
		while (input != "stop")
		{
			input = getInput();
			if (input.equalsIgnoreCase("Continue"))
			{
				System.out.println("Student ID:>");
				studentId = kb.nextLine();
				System.out.println("X Co-Ord:>");
				x = kb.nextShort();
				System.out.println("Y Co-Ord:>");
				y = kb.nextShort();

				g = new Gaze(studentId, x, y, Calendar.getInstance().getTime());

				/***************************************************************************/

				DatagramSocket clientSocket = null;
				InetAddress IPAddress;
				try
				{
					clientSocket = new DatagramSocket();
					IPAddress = InetAddress.getByName(AppData.LECTURER_IP);
				} catch (SocketException | UnknownHostException e)
				{
					e.printStackTrace();
					break;
				}

				// Create byte arrays to hold data to be sent and data that has
				// been
				// received
				byte[] sendData = SerializationUtility.serialize(g);
				byte[] receiveData = new byte[1024];

				// Package data and attempt to send it to server
				DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, AppData.PORT);
				try
				{
					clientSocket.send(sendPacket);
				} catch (IOException e)
				{
					e.printStackTrace();
					closeSocket(clientSocket);
					break;
				}
				DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
				try
				{
					clientSocket.receive(receivePacket);
				} catch (IOException e)
				{
					e.printStackTrace();
					closeSocket(clientSocket);
					break;
				}

				// Unserialize and display data from server
				Gaze receivedGaze = (Gaze) SerializationUtility.unserialize(receivePacket.getData());
				System.out.println("FROM SERVER:>" + receivedGaze.toString());
				clientSocket.close();

				/***************************************************************************/
			} else if (input.equalsIgnoreCase("Exit"))
			{
				break;
			}

			printMenu();
		}
	}

	/**
	 * Takes in a string from the users keyboard
	 * 
	 * @return
	 */
	private String getInput()
	{
		return kb.nextLine();
	}

	/**
	 * Output menu to user
	 */
	private void printMenu()
	{
		System.out.println("***  Menu   ***");
		System.out.println("(1) Continue");
		System.out.println("(2) Exit\n");
		System.out.print("Type Your Command:>");
	}

	/**
	 * Prints out the header of the application
	 */
	private void printHeader()
	{
		System.out.println("/************************************************/");
		System.out.println("/\t\tStudent Gaze Tracker V1.0\t\t/");
		System.out.println("/************************************************/");

	}

	/**
	 * Closes datagramsocket connection
	 * 
	 * @param socket
	 */
	private void closeSocket(DatagramSocket socket)
	{
		if (socket != null && !socket.isClosed())
		{
			socket.close();
		}
	}

}
