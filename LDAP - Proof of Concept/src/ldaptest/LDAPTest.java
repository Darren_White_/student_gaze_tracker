package ldaptest;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

/**
 * @author Darren White
 * @version 1.1
 */
public class LDAPTest
{

	public static String INITCTX = "com.sun.jndi.ldap.LdapCtxFactory";

	public static String MY_HOST = " ldap.forumsys.com:389";

	public static String MY_SEARCHBASE = "DC=doaminName,DC=companyName,DC=com";

	public static String MY_FILTER = "(sAMAccountName=${u})";

	public static String PTE_AM_LDAP_VALUE = "LDAP://doaminName/CN=!Res-Test01,OU=Test Accounts,OU=Associates,DC=doaminName,DC=companyName,DC=com";

	public static String MGR_DN = "cn=read-only-admin,dc=example,dc=com";

	public static String MGR_PW = "password";

	/**
	 * @param args
	 * @throws NamingException
	 */
	public static void main(String[] args) throws NamingException
	{

		Hashtable env = new Hashtable(11);
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, "ldap://ldap.forumsys.com:389/cn=read-only-admin,dc=example,dc=com");

		// Authenticate as S. User and password “mysecret”
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL, "ou=riemann,dc=example,dc=com");
		env.put(Context.SECURITY_CREDENTIALS, "password");

		try
		{
			// Create initial context
			DirContext ctx = new InitialDirContext(env);

			System.out.println(ctx.getAttributes(""));

			// Close the context when we’re done
			ctx.close();
		} catch (NamingException e)
		{
			e.printStackTrace();
		}

	}

}
