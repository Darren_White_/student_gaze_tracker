package semi.transparent.overlay.proof.of.concept.v1.pkg0;


import com.sun.awt.AWTUtilities;
import java.util.Arrays;
import com.sun.jna.*;
import com.sun.jna.platform.WindowUtils;
import com.sun.jna.platform.win32.WinDef;
import com.sun.jna.platform.win32.WinDef.HWND;
import com.sun.jna.platform.win32.WinUser;
import com.sun.jna.win32.*;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class MainApp {


   public interface User32 extends StdCallLibrary {
      User32 INSTANCE = (User32) Native.loadLibrary("user32", User32.class,
               W32APIOptions.DEFAULT_OPTIONS);

      HWND FindWindow(String lpClassName, String lpWindowName);

      int GetWindowRect(HWND handle, int[] rect);
   }

   public static int[] getRect(String windowName) throws WindowNotFoundException,
            GetWindowRectException {
      HWND hwnd = User32.INSTANCE.FindWindow(null, windowName);
      if (hwnd == null) {
         throw new WindowNotFoundException("", windowName);
      }

      int[] rect = {0, 0, 0, 0};
      int result = User32.INSTANCE.GetWindowRect(hwnd, rect);
      if (result == 0) {
         throw new GetWindowRectException(windowName);
      }
      return rect;
   }

   @SuppressWarnings("serial")
   public static class WindowNotFoundException extends Exception {
      public WindowNotFoundException(String className, String windowName) {
         super(String.format("Window null for className: %s; windowName: %s", 
                  className, windowName));
      }
   }

   @SuppressWarnings("serial")
   public static class GetWindowRectException extends Exception {
      public GetWindowRectException(String windowName) {
         super("Window Rect not found for " + windowName);
      }
   }

   public static void main(String[] args) {
      String windowName = "Semi Transparent Overlay - Proof of Concept - V1.0 - Netbeans IDE 8.1";
      int[] rect;
      try {
         rect = MainApp.getRect(windowName);
         System.out.printf("The corner locations for the window \"%s\" are %s", 
                  windowName, Arrays.toString(rect));
         displayOverlay(rect);
      } catch (MainApp.WindowNotFoundException e) {
         e.printStackTrace();
      } catch (MainApp.GetWindowRectException e) {
         e.printStackTrace();
      }      
   }
   
   private static void displayOverlay(int[] rect)
   {
       JFrame frame = new JFrame("Hello!!");
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		double width = screenSize.getWidth();
		double height = screenSize.getHeight();
		

		// Set's the window to be "always on top"
		frame.setAlwaysOnTop(true);

		frame.setLocationByPlatform(true);
		frame.add(new JLabel("  Isn't this annoying?"));
		
		// Removes title bar & users ability to move + resize
		frame.setUndecorated(true);
		frame.pack();
		
		frame.setVisible(true);
		frame.setBounds(rect[0]+10, rect[1]+5, rect[2] - rect[0] -20, rect[3] - rect[1]-20);
		frame.setOpacity((float) 0.7);
                
                frame.setAlwaysOnTop(true);
                AWTUtilities.setWindowOpaque(frame, false);
                AWTUtilities.setWindowOpacity(frame, 0.8f);
 
                
                
                
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
   }
   
   private static void paintComponent(Graphics g) {
              paintComponent(g);
              g.setColor(Color.orange);
              g.drawRect(20, 20, 100, 60);
           }
}