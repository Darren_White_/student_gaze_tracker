/********************************************************************/
/*				Contents of Repository								*/
/********************************************************************/
1) Get Screen Capture - Proof of Concept
	This project is a working code experiment used to test functionality of screen capture mechanism.
	There is no UI component to this project, there is console output and screen captures must be visually
	inspected via save directory. This project does not use any external libraries and should run without 
	need for configuration
2) LDAP - Proof of Concept
	This project does not function, the code is very buggy. It was intended to be used to test my login mechanism,
	this project is being kept as a placeholder for actual future login testing.
3) Open Window Name Finder - Proof of Concept
	Application to find the name of all open program windows on the local machine. This project finds all
	services and processes running in the background. Project uses JNA, Jar files included in build path
	, should not require further configuration
4) Student Gaze Tracker - Client - v1.0
	1 of 2 main packages where functioning code is merged into from other code experiments. IP needs to be manually 
	entered before running [IP of server]. JNA library included in build path
5) Student Gaze Tracker - Server - v1.0
	2 of 2 main packages where functioning code is merged into from other code experiments. JNA library included 
	in build path
6) ClientBrowserDisplay-Alpha
	Web based project containing html, css and js files necessary to refresh images in a client browser. Code should 
	run from index.html page. [Project regularly uploaded to hosted domain]
7) Gaze Tracking Example - Alpha
	Placeholder project to be used to get a basic gaze tracking example implemented before pulling working code
	into the main client project.
8) Client - User Interface Displays - Alpha
	This project contains the swing UI components intended for use on the students application
9) Student Gaze Tracker - Client - v2.0
	This project is the new main client project for the alpha release. It includes the functionality currently implemented
	as well as the functional user interface components
10) Student Gaze Tracker - Server - V2.0
	This project is the new main server project for the alpha release. It includes the functionality currently implemented
	as well as the functional user interface components
11) Semi Transparent Overlay - Proof of Concept
	Code experiment project that tested the feasibility of creating a semi transparent overlay application using JNA and swing
12) Server - User Interface Displays - Alpha
	This project contains the swing UI components intended for use on the lecturers application
13) Server - Screen Capture FTP - Example
	Project to test the feasibility of uploading the server screen capture image to a local server, will be replace with remote
	server for  beta. This project contains bugs.
14) Student Gaze Tracker - Client - v2.0
	Version 2.0 of the client SGT project. Iteration between alpha and beta release
15) Student Gaze Tracker - Server - v2.0
	Version 2.0 of the server SGT project. Iteration between alpha and beta release. Functions more dynamically as there are less harcoded values.